import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CONFIG } from '../core/config';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseService } from './base.service';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class StripeService extends BaseService {
    constructor(private http: HttpClient,
        private authService: AuthService) {
        super();
    }

    headers = new HttpHeaders({
        'Authorization': `Bearer ${this.authService.getToken()}`,
    });

    postStripePayment(stripeData): Observable<any> {
        return this.http.post<any>(CONFIG.base_url + "stripe/charge", stripeData, { headers: this.headers }).pipe(
            catchError(this.handleError)
        );
    }
}