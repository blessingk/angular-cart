import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MzIconModule, MzIconMdiModule, MzButtonModule, MzTabModule, MzSelectModule, MzRadioButtonModule, MzCheckboxModule, MzSwitchModule, MzModalModule, MzBadgeModule, MzCardModule, MzInputModule, MzSidenavModule, MzPaginationModule, MzTextareaModule, MzDatepickerModule, MzTooltipModule, MzDropdownModule, MzToastModule } from 'ngx-materialize';

@NgModule({
  imports: [
    CommonModule,
    MzIconModule,
    MzIconMdiModule,
    MzButtonModule,
    MzTabModule,
    MzSelectModule,
    MzRadioButtonModule,
    MzCheckboxModule,
    MzSwitchModule,
    MzModalModule,
    MzBadgeModule,
    MzCardModule,
    MzInputModule,
    MzSidenavModule,
    MzPaginationModule,
    MzTextareaModule,
    MzDatepickerModule,
    MzTooltipModule,
    MzDropdownModule,
    MzToastModule
  ],
  exports: [
    MzIconModule,
    MzIconMdiModule,
    MzButtonModule,
    MzTabModule,
    MzSelectModule,
    MzRadioButtonModule,
    MzCheckboxModule,
    MzSwitchModule,
    MzModalModule,
    MzBadgeModule,
    MzCardModule,
    MzInputModule,
    MzSidenavModule,
    MzPaginationModule,
    MzTextareaModule,
    MzDatepickerModule,
    MzTooltipModule,
    MzDropdownModule,
    MzToastModule
  ]
})
export class MaterialModule { }