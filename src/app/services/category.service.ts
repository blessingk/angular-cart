import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from '../core/config';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseService } from './base.service';

@Injectable({
    providedIn: 'root'
})
export class CategoryService extends BaseService {
    constructor(private http: HttpClient) {
        super();
    }

    getAllCategories(): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "categories").pipe(
            catchError(this.handleError)
        );
    }

    getSingleCategory(category_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "categories/" + category_id).pipe(
            catchError(this.handleError)
        );
    }

    getProductCategory(product_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "categories/inProduct/" + product_id).pipe(
            catchError(this.handleError)
        );
    }

    getDepartmentCategories(department_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "categories/inDepartment/" + department_id).pipe(
            catchError(this.handleError)
        );
    }
}