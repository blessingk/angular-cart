import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { DepartmentService, CategoryService, ShoppingCartService, AuthService } from 'src/app/services';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ShoppingcartComponent } from '../../modals/shoppingcart/shoppingcart.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Output() navSearch: EventEmitter<any> = new EventEmitter();
  @ViewChild(ShoppingcartComponent, { static: false }) shoppingcartComponent: ShoppingcartComponent;
  departments: any = [];
  searchForm: FormGroup;
  cart_items: number = 0;
  isAuthenticated: boolean = false;
  cart_id: string = '';
  constructor(private departmentService: DepartmentService,
    private categoryService: CategoryService,
    private _fb: FormBuilder,
    private shoppingcartService: ShoppingCartService,
    private authService: AuthService) { }

  ngOnInit() {
    this.departmentService.getAllDepartments().subscribe(res => {
      this.departments = res;
      this.departments.forEach(department => {
        this.categoryService.getDepartmentCategories(department.department_id).subscribe(res => {
          department.categories = res;
        })
      });
    });

    if (this.authService.isAuthenticated()) {
      this.isAuthenticated = true;
      this.shoppingcartService.generateCartID().subscribe(res => {
        localStorage.setItem('cart_id', res);
        this.cart_id = res;
        this.shoppingcartService.getShoppingCartProducts(this.cart_id).subscribe(result => {
          result.forEach(cart => {
            this.cart_items += cart.quantity;
          });
        });
      });
    }

    this.searchForm = this._fb.group({
      keyword: ['']
    })
  }

  onSearch(keyword: FormGroup) {
    this.navSearch.emit(this.searchForm.value);
  }
  onCloseSearch() {
    this.searchForm.reset();
  }

  onShowCartItems(cart_id) {
    this.cart_items = 0;
    this.shoppingcartService.getShoppingCartProducts(cart_id).subscribe(result => {
      result.forEach(cart => {
        this.cart_items += cart.quantity;
      });
    });
  }

  showCart() {
    var cart_id = localStorage.getItem('cart_id');
    this.shoppingcartComponent.open(cart_id);
  }
}
