import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from '../core/config';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseService } from './base.service';

@Injectable({
    providedIn: 'root'
})
export class FacebookAuthService extends BaseService {
    constructor(private http: HttpClient) {
        super();
    }

    facebookLogin(access_token): Observable<any> {
        return this.http.post<any>(CONFIG.base_url + "customers/facebook", access_token).pipe(
            catchError(this.handleError)
        );
    }

}