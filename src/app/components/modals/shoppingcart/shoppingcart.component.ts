import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ShoppingCartService, AuthService } from 'src/app/services';
import { MzModalComponent } from 'ngx-materialize';
import { CONFIG } from '../../../core/config';

@Component({
  selector: 'app-shoppingcart',
  templateUrl: './shoppingcart.component.html',
  styleUrls: ['./shoppingcart.component.css']
})
export class ShoppingcartComponent implements OnInit {
  @ViewChild('cartModal', { static: false }) cartModal: MzModalComponent;
  cart_items: any = [];
  shoppingcartForm: FormGroup;
  public imageBaseUrl = CONFIG.imageBaseUrl;
  isAuthenticated: boolean = false;
  quantity: number = 0;
  cart_total: number = 0;

  constructor(private _fb: FormBuilder,
    private shoppingcartService: ShoppingCartService,
    private authService: AuthService) { }

  cart_id = localStorage.getItem('card_id');
  c

  ngOnInit() {
    this.shoppingcartForm = this._fb.group({

    });
    if (this.authService.isAuthenticated()) {
      this.isAuthenticated = true;
    }
  }

  open(cart_id) {
    this.shoppingcartService.getShoppingCartProducts(cart_id).subscribe(res => {
      this.cart_items = res;
      res.forEach(cart => {
        this.cart_total = parseFloat(cart.subtotal + this.cart_total);
      })
    });
    this.cartModal.openModal();
  }

  decreaseQuantity() {
    if (this.quantity != 0) {
      this.quantity--;
    }
    this.shoppingcartForm.patchValue({
      quantity: this.quantity
    });
  }

  increaseQuantity() {
    this.quantity++;
    this.shoppingcartForm.patchValue({
      quantity: this.quantity
    });
  }

}
