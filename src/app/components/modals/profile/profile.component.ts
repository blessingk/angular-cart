import { Component, OnInit, ViewChild } from '@angular/core';
import { MzModalComponent } from 'ngx-materialize';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  @ViewChild('profileModal', { static: false }) profileModal: MzModalComponent;
  constructor() { }

  ngOnInit() {
  }

  open() {
    this.profileModal.openModal();
  }

  close() {
    this.profileModal.closeModal();
  }

}
