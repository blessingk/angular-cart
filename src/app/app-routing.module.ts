import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { CustomerComponent } from './components/customer/customer.component';
import { AuthGuard } from './guards/auth.guard';
import { ProfileComponent } from './components/modals/profile/profile.component';
import { ProductComponent } from './components/product/product.component';
import { OrderComponent } from './components/orders/order/order.component';
import { OrdersComponent } from './components/orders/orders/orders.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'customers', component: CustomerComponent, canActivate: [AuthGuard] },
  { path: 'customer/profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'product/:product_id', component: ProductComponent },
  { path: 'orders', component: OrdersComponent, canActivate: [AuthGuard] },
  { path: 'orders/inCustomer', component: OrderComponent, canActivate: [AuthGuard] },
  { path: 'shoppingcart', component: ProfileComponent, canActivate: [AuthGuard] },


  // otherwise redirect to login
  { path: '', redirectTo: 'login', pathMatch: 'full' }
];

export const routing = RouterModule.forRoot(appRoutes, { useHash: true });
