import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class BaseService {

    constructor() { }

    protected handleError(error: HttpErrorResponse) {
        return throwError(error);
    }
}
