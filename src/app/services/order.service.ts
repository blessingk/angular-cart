import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CONFIG } from '../core/config';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseService } from './base.service';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class OrderService extends BaseService {
    constructor(private http: HttpClient,
        private authService: AuthService) {
        super();
    }

    headers = new HttpHeaders({
        'Authorization': `Bearer ${this.authService.getToken()}`,
    });

    createOrder(formData): Observable<any> {
        return this.http.post<any>(CONFIG.base_url + "orders/", formData, { headers: this.headers }).pipe(
            catchError(this.handleError)
        );
    }

    getSingleOrder(order_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "orders/" + order_id, { headers: this.headers }).pipe(
            catchError(this.handleError)
        );
    }

    getCustomerOrder(): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "orders/inCustomer", { headers: this.headers }).pipe(
            catchError(this.handleError)
        );
    }

    getOrderShortDetails(order_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "orders/shortDerails/" + order_id, { headers: this.headers }).pipe(
            catchError(this.handleError)
        );
    }

}