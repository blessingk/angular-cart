import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { routing } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToastrModule } from 'ngx-toastr';
import { AuthGuard } from './guards/auth.guard';
import { AuthService, ProductService, AttributeService, CategoryService, CustomerService, DepartmentService, FacebookAuthService, OrderService, ShippingService, ShoppingCartService, StripeService, TaxService } from './services';
import { ProductComponent } from './components/product/product.component';
import { HomeComponent } from './components/home/home.component';
import { OrderComponent } from './components/orders/order/order.component';
import { RegisterComponent } from './components/modals/register/register.component';
import { LoginComponent } from './components/modals/login/login.component';
import { CustomerComponent } from './components/customer/customer.component';
import { ProfileComponent } from './components/modals/profile/profile.component';
import { OrdersComponent } from './components/orders/orders/orders.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './modules/material/material.module';
import { NavbarComponent } from './components/partials/navbar/navbar.component';
import { FooterComponent } from './components/partials/footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxGalleryModule } from 'ngx-gallery';
import { DropdownModule } from "ngx-dropdown";
import { MzNavbarModule } from 'ngx-materialize';
import { ReactiveFormsModule } from '@angular/forms';
import { Ng5SliderModule } from 'ng5-slider';
import { TopnavComponent } from './components/partials/topnav/topnav.component';
import { NgxStarsModule } from 'ngx-stars';
import { ShoppingcartComponent } from './components/modals/shoppingcart/shoppingcart.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    HomeComponent,
    OrderComponent,
    RegisterComponent,
    LoginComponent,
    CustomerComponent,
    ProfileComponent,
    OrdersComponent,
    NavbarComponent,
    FooterComponent,
    TopnavComponent,
    ShoppingcartComponent
  ],
  imports: [
    BrowserModule,
    routing,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    NgxPaginationModule,
    NgxGalleryModule,
    DropdownModule,
    MzNavbarModule,
    ReactiveFormsModule,
    Ng5SliderModule,
    NgxStarsModule
  ],
  providers: [
    AuthGuard,
    AuthService,
    ProductService,
    AttributeService,
    CategoryService,
    CustomerService,
    DepartmentService,
    FacebookAuthService,
    OrderService,
    ShippingService,
    ShoppingCartService,
    StripeService,
    TaxService
  ],
  bootstrap: [AppComponent],
  exports: [ToastrModule]
})
export class AppModule { }
