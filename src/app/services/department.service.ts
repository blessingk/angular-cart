import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from '../core/config';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseService } from './base.service';

@Injectable({
    providedIn: 'root'
})
export class DepartmentService extends BaseService {
    constructor(private http: HttpClient) {
        super();
    }

    getAllDepartments(): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "departments").pipe(
            catchError(this.handleError)
        );
    }

    getSingleDepartment(department_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "departments/" + department_id).pipe(
            catchError(this.handleError)
        );
    }
}