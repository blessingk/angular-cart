import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CONFIG } from '../core/config';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseService } from './base.service';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class ProductService extends BaseService {
    constructor(private http: HttpClient,
        private authService: AuthService) {
        super();
    }

    headers = new HttpHeaders({
        'Authorization': `Bearer ${this.authService.getToken()}`,
    });

    getAllProducts(): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "products").pipe(
            catchError(this.handleError)
        );
    }

    searchProducts(search): Observable<any> {
        let query_string = "keyword=" + search.keyword + "&" + "min_price=" + search.min_price +
            "&" + "max_price=" + search.max_price;
        return this.http.get<any>(CONFIG.base_url + "products/search/products?" + query_string).pipe(
            catchError(this.handleError)
        );
    }

    getSingleProduct(product_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "products/" + product_id).pipe(
            catchError(this.handleError)
        );
    }

    getProductsInCategory(category_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "products/inCategory/" + category_id).pipe(
            catchError(this.handleError)
        );
    }

    getProductsInDepartment(department_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "products/inDepartment/" + department_id).pipe(
            catchError(this.handleError)
        );
    }

    getProductReviews(product_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "products/" + product_id + "/reviews").pipe(
            catchError(this.handleError)
        );
    }

    postProductReview(product_id, formData): Observable<any> {
        return this.http.post<any>(CONFIG.base_url + "products/" + product_id + "/reviews", formData, { headers: this.headers }).pipe(
            catchError(this.handleError)
        );
    }

    getProductDetails(product_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "products/" + product_id + "/details").pipe(
            catchError(this.handleError),
        );
    }

    getProductLocations(product_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "products/" + product_id + "/locations").pipe(
            catchError(this.handleError)
        );
    }

}