import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CONFIG } from '../core/config';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseService } from './base.service';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class ShoppingCartService extends BaseService {
    constructor(private http: HttpClient,
        private authService: AuthService) {
        super();
    }
    headers = new HttpHeaders({
        'Authorization': `Bearer ${this.authService.getToken()}`,
    });

    generateCartID(): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "shoppingcart/generateUniqueId", { headers: this.headers }).pipe(
            catchError(this.handleError)
        );
    }

    addProductToShoppingCart(productData): Observable<any> {
        return this.http.post<any>(CONFIG.base_url + "shoppingcart/add", productData, { headers: this.headers }).pipe(
            catchError(this.handleError)
        );
    }

    getShoppingCartProducts(cart_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "shoppingcart/" + cart_id, { headers: this.headers }).pipe(
            catchError(this.handleError)
        );
    }

    updateCartItem(item_id, itemData): Observable<any> {
        return this.http.put<any>(CONFIG.base_url + "shoppingcart/update/" + item_id, itemData, { headers: this.headers }).pipe(
            catchError(this.handleError)
        );
    }

    emptyShoppingCart(cart_id): Observable<any> {
        return this.http.delete<any>(CONFIG.base_url + "shoppingcart/empty/" + cart_id, { headers: this.headers }).pipe(
            catchError(this.handleError)
        );
    }

    removeShoppingCartItem(item_id): Observable<any> {
        return this.http.delete<any>(CONFIG.base_url + "shoppingcart/removeProduct/" + item_id, { headers: this.headers }).pipe(
            catchError(this.handleError)
        );
    }
}