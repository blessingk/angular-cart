import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { CONFIG } from '../core/config';
import { Observable } from 'rxjs';
import { BaseService } from './base.service';
import { FormGroup } from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class AuthService extends BaseService {
    constructor(private http: HttpClient) {
        super();
    }

    public login(email: string, password: string): Observable<any> {
        return this.http.post<any>(CONFIG.base_url + "customers/login", {
            email: email,
            password: password
        }).pipe(
            tap(response => {
                localStorage.setItem('token', response.accessToken)
                localStorage.setItem('user', JSON.stringify(response.customer))
            }),
            catchError(this.handleError)
        )
    }

    logout() {
        // remove user and token from local storage to log user out
        localStorage.removeItem('user');
        localStorage.removeItem('token');
        localStorage.removeItem('cart_id');
    }

    get Customer() {
        return JSON.parse(localStorage.getItem('user'));
    }

    public getToken() {
        return localStorage.getItem('token');
    }

    public isAuthenticated() {
        return this.getToken();
    }

    public resetPasswordLink(form: FormGroup): Observable<any> {
        return this.http.post<any>(CONFIG.base_url + "reset/email", form.value).pipe(
            catchError(this.handleError)
        )
    }

    public passwordReset(form: FormGroup): Observable<any> {
        return this.http.post<any>(CONFIG.base_url + "password/reset", form.value).pipe(
            tap(response => {
                localStorage.setItem('token', response.token)
                localStorage.setItem('user', JSON.stringify(response.user))
            }),
            catchError(this.handleError)
        )
    }
}