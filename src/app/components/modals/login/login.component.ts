import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/services';
import { MzModalComponent, MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('loginModal', { static: false }) loginModal: MzModalComponent;
  @Output() loggedIn: EventEmitter<any> = new EventEmitter();
  loginForm: FormGroup;
  constructor(private _fb: FormBuilder,
    private authService: AuthService,
    private toastrService: MzToastService) { }

  ngOnInit() {
    this.loginForm = this._fb.group({
      email: [''],
      password: ['']
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.authService.login(this.f.email.value, this.f.password.value).subscribe(res => {
      this.loggedIn.emit();
      this.loginModal.closeModal();
      this.loginForm.reset();
    },
      error => {
        this.toastrService.show(error.error.error, 4000, 'red');
      })
  }

  open() {
    this.loginModal.openModal();
  }

  close() {
    this.loginModal.closeModal();
  }

}
