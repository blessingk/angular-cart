import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from '../core/config';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseService } from './base.service';

@Injectable({
    providedIn: 'root'
})
export class TaxService extends BaseService {
    constructor(private http: HttpClient) {
        super();
    }

    getAllTaxes(): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "tax").pipe(
            catchError(this.handleError)
        );
    }

    getSingleTax(tax_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "tax/" + tax_id).pipe(
            catchError(this.handleError)
        );
    }
}