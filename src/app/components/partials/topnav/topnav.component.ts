import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services';
import { LoginComponent } from '../../modals/login/login.component';
import { RegisterComponent } from '../../modals/register/register.component';
import { ProfileComponent } from '../../modals/profile/profile.component';

@Component({
  selector: 'app-topnav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.component.css']
})
export class TopnavComponent implements OnInit {

  @ViewChild(LoginComponent, { static: false }) loginComponent: LoginComponent;
  @ViewChild(RegisterComponent, { static: false }) registerComponent: RegisterComponent;
  @ViewChild(ProfileComponent, { static: false }) profileCompnent: ProfileComponent;

  isAuthenticated: boolean = false;
  constructor(private authService: AuthService) { }

  currentUser = this.authService.Customer;
  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      this.isAuthenticated = true;
    }
  }

  onSignIn() {
    this.loginComponent.open();
  }

  onRegister() {
    this.registerComponent.open();
  }

  onProfile() {
    this.profileCompnent.open();
  }

  logout() {
    this.authService.logout();
    this.isAuthenticated = false;
    this.currentUser = null;
  }

  loggedIn() {
    this.isAuthenticated = true;
    this.currentUser = this.authService.Customer;
  }

}
