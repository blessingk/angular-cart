import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CONFIG } from '../core/config';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseService } from './base.service';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class CustomerService extends BaseService {
    constructor(private http: HttpClient,
        private authService: AuthService) {
        super();
    }

    headers = new HttpHeaders({
        'Authorization': `Bearer ${this.authService.getToken()}`,
    });

    createCustomer(formData): Observable<any> {
        return this.http.post<any>(CONFIG.base_url + "customers", formData).pipe(
            catchError(this.handleError)
        );
    }

    getSingleCustomer(customer_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "customers/" + customer_id, { headers: this.headers }).pipe(
            catchError(this.handleError)
        );
    }

    updateCustomerDetails(formData): Observable<any> {
        return this.http.put<any>(CONFIG.base_url + "customer/", formData, { headers: this.headers }).pipe(
            catchError(this.handleError)
        );
    }

    updateCustomerAddress(formData): Observable<any> {
        return this.http.put<any>(CONFIG.base_url + "customer/address", formData, { headers: this.headers }).pipe(
            catchError(this.handleError)
        );
    }

    updateCustomerCreditCard(formData): Observable<any> {
        return this.http.put<any>(CONFIG.base_url + "products/", formData, { headers: this.headers }).pipe(
            catchError(this.handleError)
        );
    }
}