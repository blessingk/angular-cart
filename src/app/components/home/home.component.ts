import { Component, OnInit } from '@angular/core';
import { ProductService, AttributeService } from 'src/app/services';
import { CONFIG } from '../../core/config';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Options, LabelType } from 'ng5-slider';
import { MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  products: any = [];
  page: number = 1;
  attributes: any = [];
  filterForm: FormGroup;
  public imageBaseUrl = CONFIG.imageBaseUrl;

  minValue: number = 0;
  maxValue: number = 500;
  options: Options = {
    floor: 0,
    ceil: 500,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '£ ' + value;
        case LabelType.High:
          return '£ ' + value;
        default:
          return '£ ' + value;
      }
    }
  };

  constructor(
    private productService: ProductService,
    private attributeService: AttributeService,
    private _fb: FormBuilder,
    private toastService: MzToastService
  ) { }

  ngOnInit() {
    this.filterForm = this._fb.group({
      size: [''],
      keyword: [''],
      color: [''],
      min_price: [''],
      max_price: ['']
    });

    this.productService.getAllProducts().subscribe(res => {
      this.products = res.rows;
    });

    this.attributeService.getAllAttributes().subscribe(res => {
      this.attributes = res;
      this.attributes.forEach(attribute => {
        this.attributeService.getAllAttributeValues(attribute.attribute_id).subscribe(result => {
          attribute.attribute_values = result;
        })
      });
    });
  }

  navSearch(search) {
    this.productService.searchProducts(search).subscribe(res => {
      this.products = res.rows;
    })
  }

  getColor(color) {
    (2)
    switch (color) {
      case 'White':
        return 'grey';
      case 'Red':
        return 'red';
      case 'Blue':
      case 'Black':
        return 'black';
      case 'Orange':
        return 'orange';
      case 'Yellow':
        return 'yellow';
      case 'Green':
        return 'green';
      case 'Indigo':
        return 'indigo';
      case 'Purple':
        return 'purple';
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.filterForm.controls; }

  onSubmit() {
    this.filterForm.patchValue({
      min_price: this.minValue,
      max_price: this.maxValue,
    });

    if (this.f.keyword.value == '') {
      this.toastService.show('Please enter search keyword.', 4000, 'red');
      return;
    }

    this.productService.searchProducts(this.filterForm.value).subscribe(res => {
      this.products = res.rows;
    });
  }

  resetFilters(e) {
    e.stopPropagation();
    this.filterForm.reset();
    this.minValue = 0;
    this.maxValue = 500;

    this.productService.getAllProducts().subscribe(res => {
      this.products = res.rows;
    });
  }
}
