import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from '../core/config';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseService } from './base.service';

@Injectable({
    providedIn: 'root'
})
export class AttributeService extends BaseService {
    constructor(private http: HttpClient) {
        super();
    }

    getAllAttributes(): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "attributes").pipe(
            catchError(this.handleError)
        );
    }

    getSingleAttribute(attribute_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "attributes/" + attribute_id).pipe(
            catchError(this.handleError)
        );
    }

    getAllAttributeValues(attribute_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "attributes/values/" + attribute_id).pipe(
            catchError(this.handleError)
        );
    }

    getProductAttributes(product_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "attributes/inProduct/" + product_id).pipe(
            catchError(this.handleError)
        );
    }
}