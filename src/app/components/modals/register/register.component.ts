import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomerService } from 'src/app/services';
import { MzModalComponent, MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @ViewChild('registerModal', { static: false }) registerModal: MzModalComponent;
  @Output() showLogin: EventEmitter<any> = new EventEmitter();
  @Output() registered: EventEmitter<any> = new EventEmitter();
  registerForm: FormGroup;
  constructor(private _fb: FormBuilder,
    private customerService: CustomerService,
    private toastrService: MzToastService) { }

  ngOnInit() {
    this.registerForm = this._fb.group({
      name: ['',],
      email: ['',],
      password: ['',]
    });
  }

  open() {
    this.registerModal.openModal();
  }

  close() {
    this.registerModal.closeModal();
  }

  goToLogin(e) {
    e.stopPropagation();
    this.showLogin.emit();
    this.registerModal.closeModal();
  }
  onSubmit() {

    this.customerService.createCustomer(this.registerForm.value).subscribe(
      response => {
        localStorage.setItem('token', response.accessToken)
        localStorage.setItem('user', JSON.stringify(response.customer))
        this.registered.emit();
        this.registerModal.closeModal();
        this.registerForm.reset();
      },
      error => {
        this.toastrService.show(error.error.error, 4000, '');
      });
  }

}
