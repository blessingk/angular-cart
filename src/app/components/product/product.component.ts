import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductService, AuthService, AttributeService, ShoppingCartService } from 'src/app/services';
import { ActivatedRoute } from '@angular/router';
import { NgxGalleryImage, NgxGalleryOptions, NgxGalleryAnimation } from 'ngx-gallery';
import { CONFIG } from '../../core/config';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MzToastService } from 'ngx-materialize';
import { NavbarComponent } from '../partials/navbar/navbar.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  @ViewChild(NavbarComponent, { static: false }) navbarComponent: NavbarComponent;
  shoppingcartForm: FormGroup;
  reviewForm: FormGroup;
  product: any;
  product_id: number;
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  product_images: any = [];
  attributes: any = [];
  quantity: number = 0;
  reviews: any = [];
  review_avarage: number = 0;
  product_colors: any = [];
  product_sizes: any = [];
  cart_items: number = 0;

  public imageBaseUrl = CONFIG.imageBaseUrl;
  public isAuthenticated: boolean = false;

  constructor(
    private route_param: ActivatedRoute,
    private productService: ProductService,
    private authService: AuthService,
    private _fb: FormBuilder,
    private attributeService: AttributeService,
    private shoppingcartService: ShoppingCartService,
    private toastService: MzToastService) { }

  currentUser = this.authService.Customer;
  cart_id = localStorage.getItem('cart_id');
  ngOnInit() {
    this.product_id = +this.route_param.snapshot.params['product_id'];
    if (this.authService.isAuthenticated()) {
      this.isAuthenticated = true;
    }

    this.shoppingcartForm = this._fb.group({
      quantity: [1],
      product_id: [this.product_id],
      product_color: [''],
      attributes: ['s, red'],
      cart_id: this.cart_id
    });

    this.reviewForm = this._fb.group({
      review: [''],
      rating: ['']
    });

    this.attributeService.getProductAttributes(this.product_id).subscribe(res => {
      this.attributes = res;
      this.attributes.forEach(attribute => {
        if (attribute.attribute_name == 'Color') {
          this.product_colors.push({
            id: attribute.attribute_value_id,
            value: attribute.attribute_value
          });
        } else if (attribute.attribute_name == 'Size') {
          this.product_sizes.push({
            id: attribute.attribute_value_id,
            value: attribute.attribute_value
          });
        }
      });
    });

    this.productService.getSingleProduct(this.product_id).subscribe(res => {
      this.product = res;
      this.product_images.push({
        small: this.imageBaseUrl + this.product.image,
        medium: this.imageBaseUrl + this.product.image,
        big: this.imageBaseUrl + this.product.image
      });
      this.product_images.push({
        small: this.imageBaseUrl + this.product.image_2,
        medium: this.imageBaseUrl + this.product.image_2,
        big: this.imageBaseUrl + this.product.image_2
      });
      this.galleryOptions = [
        {
          width: '600px',
          height: '400px',
          thumbnailsColumns: 4,
          imageAnimation: NgxGalleryAnimation.Slide,
          imageArrows: true,
          preview: true
        },
        // max-width 800
        {
          breakpoint: 800,
          width: '100%',
          height: '600px',
          imagePercent: 80,
          thumbnailsPercent: 20,
          thumbnailsMargin: 20,
          thumbnailMargin: 20
        },
        // max-width 400
        {
          breakpoint: 400,
          preview: false
        }
      ];

      this.galleryImages = this.product_images;

      this.productService.getProductReviews(res.product_id).subscribe(res => {
        this.reviews = res;
        this.reviews.forEach(review => {
          this.review_avarage += review.rating;
        });
        this.product.review = (this.review_avarage / res.length);
      });
    });

  }

  decreaseQuantity() {
    if (this.quantity != 0) {
      this.quantity--;
    }
    this.shoppingcartForm.patchValue({
      quantity: this.quantity
    });
  }

  increaseQuantity() {
    this.quantity++;
    this.shoppingcartForm.patchValue({
      quantity: this.quantity
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.shoppingcartForm.controls; }

  onSubmit() {
    if (this.f.quantity.value == '' || this.f.quantity.value == 0) {
      this.toastService.show('Quantity can not be zero or empty.', 4000, 'red');
      return;
    }
    this.shoppingcartService.addProductToShoppingCart(this.shoppingcartForm.value).subscribe(res => {
      this.toastService.show('Cart item added.', 4000, 'green');
      this.navbarComponent.onShowCartItems(this.cart_id);
    }, error => {
      this.toastService.show(error.erro.error, 4000, 'red');
    });
  }

  // convenience getter for easy access to form fields
  get reviewF() { return this.reviewForm.controls; }

  onPostReview() {
    if (this.reviewF.rating.value == '' || this.reviewF.review.value == '') {
      this.toastService.show('Review and rating fields are required.', 4000, 'red');
    }

    this.productService.postProductReview(this.product_id, this.reviewForm.value).subscribe(res => {
      this.toastService.show('Review submitted successfully.', 4000, 'green');
      this.productService.getProductReviews(this.product_id).subscribe(res => {
        this.reviews = res;
        this.reviews.forEach(review => {
          this.review_avarage += review.rating;
        });
        this.product.review = (this.review_avarage / res.length);
      });
      this.reviewForm.reset();
    });

  }

  onRatingSet(rating) {
    this.reviewForm.patchValue({
      rating: rating
    });
  }

  getColor(color) {
    (2)
    switch (color) {
      case 'White':
        return 'grey';
      case 'Red':
        return 'red';
      case 'Blue':
      case 'Black':
        return 'black';
      case 'Orange':
        return 'orange';
      case 'Yellow':
        return 'yellow';
      case 'Green':
        return 'green';
      case 'Indigo':
        return 'indigo';
      case 'Purple':
        return 'purple';
    }
  }

}
