import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from '../core/config';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseService } from './base.service';

@Injectable({
    providedIn: 'root'
})
export class ShippingService extends BaseService {
    constructor(private http: HttpClient) {
        super();
    }

    getAllShippingRegions(): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "shipping/regions").pipe(
            catchError(this.handleError)
        );
    }

    getShippingsInRegion(shipping_region_id): Observable<any> {
        return this.http.get<any>(CONFIG.base_url + "shipping/regions/" + shipping_region_id).pipe(
            catchError(this.handleError)
        );
    }
}